import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import functools


class Task:
    def __init__(self, q):
        self.q = np.array(q)
        self.size = len(q)
        self.q_dict = self.create_list_of_q_dict()      # dictionary of coordinates

    def create_list_of_q_dict(self):
        q_dict = [{}, {}]
        for i in range(2):
            dict_of_q = dict()
            for j in range(self.size):
                dict_of_q[f'A{j + 1}'] = self.q[j][i]
            q_dict[i] = dict_of_q
            del dict_of_q
        return q_dict

    def create_annotations_for_graph(self):
        annotations = ['None'] * self.size
        for i in range(self.size):
            for j in range(self.size):
                if functools.reduce(lambda x, y: x and y,
                                    map(lambda p, q: p == q, self.q[i], self.q[j]), True):
                    if annotations[i] == 'None':
                        annotations[i] = f'A{j + 1}'
                    else:
                        annotations[i] += f'=A{j + 1}'
        return annotations

    def calc_pareto_slater(self, choice):
        population_ids = np.arange(self.size)
        front = np.ones(self.size, dtype=bool)

        if choice == 'pareto' or choice == 'p':
            for i in range(self.size):
                for j in range(self.size):
                    # noinspection PyTypeChecker
                    if all(self.q[j] >= self.q[i]) and any(self.q[j] > self.q[i]):
                        front[i] = 0
                        break
        else:
            for i in range(self.size):
                for j in range(self.size):
                    # noinspection PyTypeChecker
                    if all(self.q[j] > self.q[i]):
                        front[i] = 0
                        break
        # Return ids of scenarios on pareto front
        return population_ids[front]

    def calc(self):
        pareto_pairs, slater_pairs = dict(), dict()
        q1 = [self.q[i][0] for i in range(self.size)]
        q2 = [self.q[i][1] for i in range(self.size)]

        # Loop through each item. This will then be compared with all other items
        for i in range(self.size):
            pareto_pairs[f'A{i+1}'], slater_pairs[f'A{i+1}'] = '-', '-'
            # Loop through all other items
            for j in range(self.size):
                if j != i:
                    if q1[i] < q1[j] and q2[i] < q2[j]:
                        pareto_pairs[f'A{i+1}'] = f'A{j+1}'
                        slater_pairs[f'A{i+1}'] = f'A{j+1}'
                        break
                    elif (q1[i] < q1[j] and q2[i] <= q2[j]) \
                            or (q1[i] <= q1[j] and q2[i] < q2[j]):
                        pareto_pairs[f'A{i+1}'] = f'A{j+1}'
                        break

        return pareto_pairs, slater_pairs

    def print_result(self, index, plot=True):
        pareto_pairs, slater_pairs = self.calc()
        self.print_result_table(index, pareto_pairs, slater_pairs)
        if plot:
            self.print_front(index, 'p')
            self.print_front(index, 's')

    def print_result_table(self, index, pareto_pairs, slater_pairs):
        df = pd.DataFrame([self.q_dict[0], self.q_dict[1], pareto_pairs, slater_pairs],
                          index=['Q1', 'Q2', 'P', 'S'])

        pd.set_option('display.max_columns', 500)
        pd.set_option('display.width', 1000)
        print('\n\nTask {0}\n\n{1}'.format(index, df))

    def print_front(self, index, choice='pareto'):
        x, y = self.q[:, 0], self.q[:, 1]
        x_front, y_front = self.build_front(choice)
        annotations = self.create_annotations_for_graph()

        for i, key in enumerate(annotations):
            plt.annotate(key, (x[i], y[i]), xytext=(x[i], y[i] + 0.15),
                         xycoords='data', textcoords='data', fontsize=7.5)

        plt.scatter(x, y)
        plt.plot(x_front, y_front, color='orange', marker='o', markerfacecolor='w')

        plt.xlabel('Q1')
        plt.ylabel('Q2')
        if choice == 'pareto' or choice == 'p':
            plt.title(f'Pareto Frontier for Task {index}')
        else:
            plt.title(f'Slater Frontier for Task {index}')
        plt.show()

    def build_front(self, choice='pareto'):
        indexes = self.calc_pareto_slater(choice)
        front = self.q[list(indexes)]

        front_df = pd.DataFrame(front)
        front_df.sort_values(0, inplace=True)
        front_df.sort_values(1, inplace=True, ascending=False)
        front = front_df.values
        return front[:, 0], front[:, 1]
